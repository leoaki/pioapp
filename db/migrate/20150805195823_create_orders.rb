class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :Co_Pedido
      t.decimal :Sa_Subtotal
      t.decimal :Sa_Igv
      t.decimal :Sa_Neto

      t.timestamps null: false
    end
  end
end
