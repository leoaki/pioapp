class CreateClientes < ActiveRecord::Migration
  def change
    create_table :clientes do |t|
      t.string :Co_Cliente
      t.string :Nu_Dni
      t.string :No_Cliente
      t.string :Ape_Paterno
      t.string :Ape_Mateno
      t.date :Fe_Ingreso

      t.timestamps null: false
    end
  end
end
