class CreateMesas < ActiveRecord::Migration
  def change
    create_table :mesas do |t|
      t.string :Co_Mesa
      t.integer :Qt_CantidadSillas
      t.integer :Nu_Mesa
      t.string :Tipo_Mesa

      t.timestamps null: false
    end
  end
end
