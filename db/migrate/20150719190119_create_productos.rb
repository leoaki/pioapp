class CreateProductos < ActiveRecord::Migration
  def change
    create_table :productos do |t|
      t.string :Co_Producto
      t.string :No_Producto
      t.decimal :Qt_CostoProducto
      t.integer :Nu_CantidadProducto
      t.date :Fe_MovimientoProducto
      t.date :Fe_VencimientoProducto

      t.timestamps null: false
    end
  end
end
