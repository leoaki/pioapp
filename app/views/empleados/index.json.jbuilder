json.array!(@empleados) do |empleado|
  json.extract! empleado, :id, :Nu_Dni, :No_Empleado, :Tx_Direccion, :Co_Departamento, :Co_Provincia, :Co_Distrito, :Tx_Email, :Fe_Nacimiento, :Tx_telefono, :Fl_Sexo, :Fl_EstadoCivil, :Fe_Contrato, :Fe_Culminacion, :Sa_Salario
  json.url empleado_url(empleado, format: :json)
end
