json.array!(@orders) do |order|
  json.extract! order, :id, :Co_Pedido, :Sa_Subtotal, :Sa_Igv, :Sa_Neto
  json.url order_url(order, format: :json)
end
