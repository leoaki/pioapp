class ReportPdf < Prawn::Document
  def initialize(clientes)
    super()
    @clientes = clientes
    #header    
    text_content
    table_content
  end
 
 
  def text_content
    # The cursor for inserting content starts on the top left of the page. Here we move it down a little to create more space between the text and the image inserted above
    y_position = cursor - 50
    text "LISTADO DE CLIENTES"
    
  end
 
  def table_content
    # This makes a call to product_rows and gets back an array of data that will populate the columns and rows of a table
    # I then included some styling to include a header and make its text bold. I made the row background colors alternate between grey and white
    # Then I set the table column widths
    table client_rows do
      row(0).font_style = :bold
      self.header = true
      self.row_colors = ['DDDDDD', 'FFFFFF']
      self.column_widths = [80, 150, 150, 150]
    end
  end
 
  def client_rows
    [['DNI/RUC', 'Nombre', 'Apellido Paterno', 'Apellido Materno']] +
      @clientes.map do |cliente|
      [cliente.Nu_Dni, cliente.No_Cliente, cliente.Ape_Paterno, cliente.Ape_Mateno]
    end
  end
end