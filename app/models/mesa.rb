class Mesa < ActiveRecord::Base
	validates :Co_Mesa, :Qt_CantidadSillas, :Nu_Mesa, :Tipo_Mesa, presence: true
	validates :Co_Mesa, uniqueness: true
	validates :Tipo_Mesa, length: { minimum: 3}
	validates :Qt_CantidadSillas, :Nu_Mesa, numericality: { only_integer: true }
	has_many :recepcions, :dependent => :destroy
end
