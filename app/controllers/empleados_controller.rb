class EmpleadosController < ApplicationController
  before_action :set_empleado, only: [:show, :edit, :update, :destroy]

  # GET /empleados
  # GET /empleados.json
  def index
    @searchByDni = params[:dni]
    if @searchByDni
      @empleados = Empleado.where('Nu_Dni LIKE?', "%#@searchByDni%").order(:Nu_Dni).page(params[:page]).per(5)
    else
      @empleados = Empleado.order(:Nu_Dni).page(params[:page]).per(5)
    end
  end

  # GET /empleados/1
  # GET /empleados/1.json
  def more    
  end
  def show
  end

  # GET /empleados/new
  def new
    @empleado = Empleado.new
  end

  # GET /empleados/1/edit
  def edit
  end

  # POST /empleados
  # POST /empleados.json
  def create
    @empleado = Empleado.new(empleado_params)

    respond_to do |format|
      if @empleado.save
        format.html { redirect_to @empleado, notice: 'Empleado fue creado satisfactoriamente.' }
        format.json { render :show, status: :created, location: @empleado }
      else
        format.html { render :new }
        format.json { render json: @empleado.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /empleados/1
  # PATCH/PUT /empleados/1.json
  def update
    respond_to do |format|
      if @empleado.update(empleado_params)
        format.html { redirect_to @empleado, notice: 'Empleado fue actualizado satisfactoriamente.' }
        format.json { render :show, status: :ok, location: @empleado }
      else
        format.html { render :edit }
        format.json { render json: @empleado.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /empleados/1
  # DELETE /empleados/1.json
  def destroy
    @empleado.destroy
    respond_to do |format|
      format.html { redirect_to empleados_url, notice: 'Empleado fue destruido satisfactoriamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_empleado
      @empleado = Empleado.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def empleado_params
      params.require(:empleado).permit(:cargo_id, :Nu_Dni, :No_Empleado, :Tx_Direccion, :Co_Departamento, :Co_Provincia, :Co_Distrito, :Tx_Email, :Fe_Nacimiento, :Tx_telefono, :Fl_Sexo, :Fl_EstadoCivil, :Fe_Contrato, :Fe_Culminacion, :Sa_Salario)
    end

  public
    #METODO PARA OBTENER EDAD Y MOSTRARLO EN LA LISTA
    def edad_empleado (empleado_id)
      @empleado = Empleado.find(empleado_id)
      age = (DateTime.now.to_date - @empleado.Fe_Nacimiento ).to_i/365
      return age
    end
  helper_method :edad_empleado
end
