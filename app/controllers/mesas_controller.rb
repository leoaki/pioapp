class MesasController < ApplicationController
  before_action :set_mesa, only: [:show, :edit, :update, :destroy]

  # GET /mesas
  # GET /mesas.json
  def index
    @searchByStatus = params[:status]
    if @searchByStatus
      @mesas = Mesa.where('free LIKE?', "%#@searchByStatus%").order(:Nu_Mesa).page(params[:page]).per(5)
    else
      @mesas = Mesa.order(:Nu_Mesa).page(params[:page]).per(5)
    end
  end

  # GET /mesas/1
  # GET /mesas/1.json
  def show
  end

  # GET /mesas/new
  def new
    @mesa = Mesa.new
  end

  # GET /mesas/1/edit
  def edit
  end

  # POST /mesas
  # POST /mesas.json
  def create
    @mesa = Mesa.new(mesa_params)
    @mesa.free = 0
    respond_to do |format|
      if @mesa.save
        format.html { redirect_to @mesa, notice: 'La mesa fue agregada con éxito.' }
        format.json { render :show, status: :created, location: @mesa }
      else
        format.html { render :new }
        format.json { render json: @mesa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mesas/1
  # PATCH/PUT /mesas/1.json
  def update
    respond_to do |format|
      if @mesa.update(mesa_params)
        format.html { redirect_to @mesa, notice: 'La mesa fué actualizada con éxito.' }
        format.json { render :show, status: :ok, location: @mesa }
      else
        format.html { render :edit }
        format.json { render json: @mesa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mesas/1
  # DELETE /mesas/1.json
  def destroy
    @mesa.destroy
    respond_to do |format|
      format.html { redirect_to mesas_url, notice: 'La Mesa se eliminó con éxito.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mesa
      @mesa = Mesa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mesa_params
      params.require(:mesa).permit(:Co_Mesa, :Qt_CantidadSillas, :Nu_Mesa, :Tipo_Mesa)
    end
end
