# PROYECTO PIO APP #

Este proyecto se realiza para el curso de DESARROLLO PARA ENTORNO WEB. Esta aplicación buscará ser una solución informática para la administración de los pedidos en las pollerías.
Para clonar el proyecto en sus PC abran la consola de GIT y digiten:
git clone https://leoaki@bitbucket.org/leoaki/pioapp.git
***https://leoaki@bitbucket.org/leoaki/pioapp.git puede variar

### COMENTARIO ###
* Cada vez que necesitemos de una gema, esta debe ser comunicada al grupo para el posterior BUNDLE INSTALL
* Utilizaremos la consola de GIT (rombo en color naranja)
* Cuando modifiquemos un archivo por consola deberemos correr el siguiente parámetro: git add, ejemplo: Supongamos que edito o agrego un nuevo archivo que se llama pepito.rb, entonces para subirlo al repositorio debemos ubicarnos en el archivo por la consola y digitar git add pepito.rb (Puedes agregar na serie archivos). Paso seguido, debemos darle un alias(commit) a esa versión mediante el comando : git commit -m 'este texto se edita, usemos terminos precisos para identificar que de nuevo tiene este commit'. Por último, debemos subir nuestro commit, lo hacemos mediante la instrucción: git push. Les pedirá una contraseña, sólo deben digitar la suya..
* Luego, si queremos tener la actualización de los cambios hechos por nuestro compañero, sólo debemos ejecutar el comando: git pull.

### DOCUMENTACIÓN ###
* Requerimientos: https://docs.google.com/document/d/1PSEKGPUtlfjYf-Y4uftw8PIkYU4OCQnZFZcDbkMDLGo/edit#heading=h.gjdgxs .
* Referencia a Bootstrap: https://docs.google.com/document/d/1Bob48blvHDEzFHeEO65C-ocSpJ0FrarFvzqXOJneb4I/edit .
* PENCIL: https://drive.google.com/open?id=0Bw_cllYiB3jhflFTUFR2WVBFbHVGWE9rb3JRRGtNNGxSN3pJN0E5TTdDMEF5VnpweDlMQmc .
* MER: https://drive.google.com/open?id=0B3-HVcOEcimxYWVmU0lJdkU3ajA

### EQUIPO DE TRABAJO ###
* Aquino Leonardo
* Chavez Erika
* Santillán Aldo