require 'test_helper'

class EmpleadosControllerTest < ActionController::TestCase
  setup do
    @empleado = empleados(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:empleados)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create empleado" do
    assert_difference('Empleado.count') do
      post :create, empleado: { Co_Departamento: @empleado.Co_Departamento, Co_Distrito: @empleado.Co_Distrito, Co_Empleado: @empleado.Co_Empleado, Co_Provincia: @empleado.Co_Provincia, Fe_Contrato: @empleado.Fe_Contrato, Fe_Culminacion: @empleado.Fe_Culminacion, Fe_Nacimiento: @empleado.Fe_Nacimiento, Fl_EstadoCivil: @empleado.Fl_EstadoCivil, Fl_Sexo: @empleado.Fl_Sexo, No_Empleado: @empleado.No_Empleado, Nu_Dni: @empleado.Nu_Dni, Sa_Salario: @empleado.Sa_Salario, Tx_Direccion: @empleado.Tx_Direccion, Tx_Email: @empleado.Tx_Email, Tx_telefono: @empleado.Tx_telefono }
    end

    assert_redirected_to empleado_path(assigns(:empleado))
  end

  test "should show empleado" do
    get :show, id: @empleado
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @empleado
    assert_response :success
  end

  test "should update empleado" do
    patch :update, id: @empleado, empleado: { Co_Departamento: @empleado.Co_Departamento, Co_Distrito: @empleado.Co_Distrito, Co_Empleado: @empleado.Co_Empleado, Co_Provincia: @empleado.Co_Provincia, Fe_Contrato: @empleado.Fe_Contrato, Fe_Culminacion: @empleado.Fe_Culminacion, Fe_Nacimiento: @empleado.Fe_Nacimiento, Fl_EstadoCivil: @empleado.Fl_EstadoCivil, Fl_Sexo: @empleado.Fl_Sexo, No_Empleado: @empleado.No_Empleado, Nu_Dni: @empleado.Nu_Dni, Sa_Salario: @empleado.Sa_Salario, Tx_Direccion: @empleado.Tx_Direccion, Tx_Email: @empleado.Tx_Email, Tx_telefono: @empleado.Tx_telefono }
    assert_redirected_to empleado_path(assigns(:empleado))
  end

  test "should destroy empleado" do
    assert_difference('Empleado.count', -1) do
      delete :destroy, id: @empleado
    end

    assert_redirected_to empleados_path
  end
end
